#!/bin/sh

set -e

for g in `find | grep .gkf$ | grep -v test/bug`
do

    F=`basename $g`               # base file name
    X=`basename -s .gkf $g`       # base name without trailing .gkf
    D=`echo $g | sed s/$F//`      # directory


    # check XML formal structure
    # --------------------------
    #
    xmllint --schema http://www.gnu.org/software/gama/gama-local $D$F --noout


    # compute XML results
    # -------------------
    #
    # bin/gama-local $g --xml $D$X.xml --algorithm gso


    # check XML results
    # -----------------
    #
    bin/gama-local $g --xml /tmp/temp.xml
    bin/check_xml_xml "example ..." /tmp/temp.xml $D$X.xml

done
