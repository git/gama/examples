/* directory structure
 * -------------------
 *
 *    results
 *       version
 *          gama-local
 *             gso
 *             svd
 *             cholesky
 *             envelope
 *          gama-g3
 *             gso
 *             svd
 *             cholesky
 *             envelope
 */

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

const char* all[] = {"gso", "svd", "cholesky", "envelope"};

std::vector<std::string> file_list;
std::ostringstream rules;

int main()
{
  std::string line;
  while (std::getline(std::cin, line))
    {
      std::istringstream algofile(line);
      char c = '#';          // skip comments and empty lines
      algofile >> c;
      if (c == '#') continue;
      algofile.putback(c);

      std::string algorithm;
      algofile >> algorithm;

      algofile >> c;
      std::string dirfile;
      dirfile = c;           // first character of the filename
      while (algofile.get(c) && c != '#' && !std::isspace(c)) dirfile += c;

      std::string program, file;

      for (unsigned int i=0; i<dirfile.length(); i++)
        {
          if (dirfile[i] == '/')
            {
              if (file == "gama-local" ||
                  file == "gama-g3") program = file;

              file.clear();

              continue;
            }

          file += dirfile[i];
        }

      unsigned int dot = 0;
      for (unsigned int i=0; i<file.length(); i++)
        {
          if (file[i] == '.') dot = i;
        }
      file = file.substr(0,dot);

      for (unsigned int i=0; i<sizeof(all)/sizeof(all[0]); i++)
        {
          std::string a;
          if (algorithm == "*") 
            a = all[i]; 
          else
            a = algorithm;

          std::string PROGRAM;
          if      (program == "gama-local")
            PROGRAM = "$(GAMALOCAL) --algorithm " + a + 
              " $< --xml $@  --text " 
              + program + "/" + a + "/" + file + ".txt";
          else if (program == "gama-g3")
            PROGRAM = "$(GAMAG3) --algorithm " + a + " $<  $@";

          std::string output = program + "/" + a + "/" + file + ".xml";
          file_list.push_back(output);

          rules << output << " : " << dirfile << " $(OTHERDEPS)\n"
                << "\t" << PROGRAM << std::endl;

          if (algorithm != "*") break;
        }
    }

  std::cout << "OTHERDEPS=\n\n"
            << "all :";
  for (unsigned int i=0; i<file_list.size(); i++)
    std::cout << " " << file_list[i];
  std::cout << "\n\n" << rules.str();
}
