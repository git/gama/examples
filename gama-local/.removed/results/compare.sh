#!/bin/bash

FILES=$(find . | egrep "$1" | egrep "$2" | egrep "$3" \
               | egrep "$4" | egrep "$5" | egrep "$6" \
               | egrep "$7" | egrep "$8" | egrep "$9" | sort)

for i in $FILES
do
  a=$(echo $i | sed s/".*\/"//)
  for j in $FILES
  do
    b=$(echo $j | sed s/".*\/"//)
    if [ "$a" == "$b" ];
    then
      if [ "$i" < "$j" ];
      then
        diff -u $i $j;
      fi
    fi
  done
done