#!/bin/sh
#
# set -e   # exit on the first error

# implicit versions 
NEW=1.17
OLD=1.16

CHECK=../../gama/tests/gama-local/scripts/check_xml_xml

if ! test -x $CHECK; then
    echo
    echo program $CHECK does not exist
    echo try \'cd ../../gama \&\& make check\' to build it
    echo
    exit 1
fi

HELP=$(echo $1 | grep \\-h)

if test $HELP"xxx" != "xxx"; then
    echo
    echo $0"            # "$0 $NEW $OLD
    echo $0" 1.xx       # "$0 1.x $OLD
    echo $0" 1.xx 1.yy"
    echo
    exit 1
fi

if test $1"xxx" != "xxx"; then
    NEW=$1
fi

if test $2"xxx" != "xxx"; then
    OLD=$2
fi

echo
echo comparing versions $NEW $OLD
echo

FILES=$(find results -type f -name *$NEW*.xml)

if ! test "$FILES""xxx" != "xxx"; then
    echo no files found
fi

for f in $FILES; do
    g=$(echo "$f" | sed s/$NEW/$OLD/g)
    if test -f $g; then
	echo "                     "$f
	$CHECK $g $f $g
    fi
done
